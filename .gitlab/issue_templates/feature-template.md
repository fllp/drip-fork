## Yeah a feature idea 🧩

### what should this feature do or solve? 🪄

Please give a short overview so as many people as possible would be able to understand.

### what is particularly important to the people who would use this feature?

Do you have certain user groups in mind?

### Any idea where it shall be placed in the app?

### is it connected with or dependent on some other feature?

### any idea how it shall look (sketch?)

feel free to attach a file 📎

### what could be difficulties (with other components) 🪆
