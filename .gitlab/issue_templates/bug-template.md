## oh no a bug 🐛

### Description what has happened

Short overview how the bug manifests.

### which OS + version is your device

- [ ] Android _number_
- [ ] iOS _number_

### which drip version number are you using

_On your phone go to ➞ menu on the top right ➞ about, scroll to the very bottom and find the version number_

### how did it happen

_what triggered the bug/behavior, always/sometimes, is it reproducible(how)?_

### describe how it looks or add screenshot

feel free to attach a file 📎

### any idea to solve it

💡
